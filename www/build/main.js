webpackJsonp([0],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_services_orden__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, toastCtrl, storage, orden) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.orden = orden;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        /*setTimeout(() => {
    
        }, 500);*/
    };
    LoginPage.prototype.verificarPlaca = function () {
        var _this = this;
        var data = { 'placa': this.placa };
        this.orden.verificarPlaca(data)
            .then(function (data) {
            console.log(data);
            _this.storage.set('placa', _this.placa);
            console.log(_this.storage);
            console.log(_this.storage.get('placa'));
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
        }).catch(function (error) {
            _this.toastCtrl.create({ message: 'No hay un vehiculo con esa placa', duration: 5000 }).present();
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\login\login.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-title>\n        Servibucaros\n      </ion-title>\n    </ion-navbar>\n  </ion-header>\n  <ion-content padding>\n    <form #loginForm="ngForm" (ngSubmit)="verificarPlaca()" autocomplete="off">\n      <ion-row>\n        <ion-col>\n          <ion-list inset>\n            <ion-item text-center>\n                <ion-img width="192" height="182" src="http://servibucaros.com/cpanel/public/images/app/logo.png" cache="true" style="border-radius: 5px;"></ion-img>\n            </ion-item>\n            <ion-item>\n                <ion-icon name="car" item-left style="color: #FF7000"></ion-icon>\n                <ion-label stacked>Placa</ion-label>\n              <ion-input placeholder="Placa del vehiculo" name="placa" id="placa" type="text" required [(ngModel)]="placa"></ion-input>\n            </ion-item>\n          </ion-list>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <div *ngIf="error" class="alert alert-danger">{{error}}</div>\n          <button ion-button round class="submit-btn btn-general" full type="submit" >Ingresar <!-- [disabled]="!loginForm.form.valid" -->\n          </button>\n        </ion-col>\n      </ion-row>\n    </form>\n  </ion-content>\n'/*ion-inline-end:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__app_services_orden__["a" /* Orden */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_services_orden__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__listadoOrdenes_listadoOrdenes__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__promociones_promociones__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__galeria_galeria__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__quienes_quienes__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__contactenos_contactenos__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, toastCtrl, storage, orden) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.orden = orden;
        this.cantidad = 0;
        console.log(this.storage);
        this.storage.get('placa').then(function (val) {
            _this.placa = val;
            _this.cargarOrdenes();
        });
    }
    HomePage.prototype.listadoOrdenes = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__listadoOrdenes_listadoOrdenes__["a" /* ListadoOrdenes */]);
    };
    HomePage.prototype.promociones = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__promociones_promociones__["a" /* Promociones */]);
    };
    HomePage.prototype.galeria = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__galeria_galeria__["a" /* Galeria */]);
    };
    HomePage.prototype.quienes = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__quienes_quienes__["a" /* Quienes */]);
    };
    HomePage.prototype.contactenos = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__contactenos_contactenos__["a" /* Contactenos */]);
    };
    HomePage.prototype.reservas = function () {
        this.toastCtrl.create({ message: 'Las reservas estan inactivas', duration: 5000 }).present();
        //this.navCtrl.push(Reservas);
    };
    HomePage.prototype.salir = function () {
        this.storage.remove('placa');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    HomePage.prototype.cargarOrdenes = function () {
        var _this = this;
        var data = { 'placa': this.placa };
        this.orden.obtenerOrdenes(data)
            .then(function (data) {
            console.log(data);
            _this.cantidad = data['ordenes'].length;
            console.log(_this.cantidad);
        }).catch(function (error) {
            _this.toastCtrl.create({ message: 'El vehiculo no tiene servicios por reclamar', duration: 7000 }).present();
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title style="width: 40%;margin: 0px;float: left;">Inicio</ion-title>\n    <ion-title style="float: left;position: relative;width: 48%;margin: 0px;float: left;text-transform: uppercase">{{ placa }}</ion-title>\n    <ion-icon id="salir" name="exit" style="float: left;position: relative;font-size: 2.5em;color:white;" (click)="salir()"></ion-icon>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="card-background-page">\n    <ion-card>\n      <img src="http://servibucaros.com/cpanel/public/images/app/servicios.png" (click)="listadoOrdenes()"/>\n      <ion-row>\n        <ion-col>\n          <button ion-button icon-right clear medium round (click)="listadoOrdenes()" style="background: #FF7000;color:white;width: 100%;">\n              <!--<ng-container *ngFor="let orden of ordenes;let indice = index">\n                  <ion-icon name="car"></ion-icon>\n              </ng-container>-->\n            <div>&nbsp;Tiene {{ cantidad }} servicios</div>\n            <ion-icon name="send"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n\n    <ion-card>\n      <img src="http://servibucaros.com/cpanel/public/images/app/promociones.png" (click)="promociones()"/>\n    </ion-card>\n\n    <ion-card>\n      <img src="http://servibucaros.com/cpanel/public/images/app/galeria.png" (click)="galeria()"/>\n    </ion-card>\n\n    <ion-card>\n      <img src="http://servibucaros.com/cpanel/public/images/app/quienes.png" (click)="quienes()"/>\n    </ion-card>\n\n    <ion-card>\n      <img src="http://servibucaros.com/cpanel/public/images/app/contactenos.png" (click)="contactenos()"/>\n    </ion-card>\n\n    <ion-fab right top>\n        <button ion-fab color="light"><ion-icon name="globe" style="color:#FF7000;font-size: 5rem;"></ion-icon></button>\n        <ion-fab-list side="bottom">\n          <a href="http://facebook.com/ServiBucaros-1003723559678825" ion-fab><ion-icon name="logo-facebook" style="color:#4267b2"></ion-icon></a>\n        </ion-fab-list>\n      </ion-fab>\n\n      <ion-fab right bottom (click)="reservas()">\n          <button ion-fab color="light"><ion-icon name="alarm" style="color:#FF7000;font-size: 5rem;"></ion-icon></button>\n      </ion-fab>\n      <!--<button ion-button icon-only round color="light"><ion-icon name="alarm" style="color: #FF7000"></ion-icon></button>-->\n</ion-content>\n'/*ion-inline-end:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__app_services_orden__["a" /* Orden */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 113:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 113;

/***/ }),

/***/ 154:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 154;

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListadoOrdenes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_services_orden__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListadoOrdenes = /** @class */ (function () {
    function ListadoOrdenes(navCtrl, toastCtrl, navParams, storage, orden) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.orden = orden;
        this.cantidad = 0;
        this.ordenes = [];
        this.storage.get('placa').then(function (val) {
            _this.placa = val;
            _this.cargarOrdenes();
        });
    }
    ListadoOrdenes.prototype.cargarOrdenes = function () {
        var _this = this;
        var data = { 'placa': this.placa };
        this.orden.obtenerOrdenes(data)
            .then(function (data) {
            console.log(data);
            _this.ordenes = data['ordenes'];
            _this.cantidad = data['ordenes'].length;
        }).catch(function (error) {
            _this.toastCtrl.create({ message: 'El vehiculo no tiene servicios por reclamar', duration: 5000 }).present();
        });
    };
    ListadoOrdenes = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-listadoOrdenes',template:/*ion-inline-start:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\listadoOrdenes\listadoOrdenes.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title style="font-size: 2rem;">\n      Listado de ordenes\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-list>\n    <ng-container *ngFor="let orden of ordenes;let indice = index">\n        <ion-list-header style="font-size: 1.7rem;margin-bottom: 0px;font-weight: 500;color: black;">\n            <label style="color: #FF7000;font-size: 1.9rem;">{{ (indice+1) }}).</label>&nbsp;\n            {{ orden.fecha }} <label style="color: #FF7000;">--</label>\n            {{ orden.nombre }}<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n            <label style="color: #FF7000;">$ </label>{{ orden.valor_total }}\n        </ion-list-header>\n        <ng-container *ngFor="let ordenServicio of orden.ordenServicios">\n            <ion-item style="font-size: 1.3rem">\n              <label style="color: #FF7000;font-size: 1.5rem;"> *</label>\n              {{ ordenServicio.nombre }}.</ion-item>\n        </ng-container>\n    </ng-container>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\listadoOrdenes\listadoOrdenes.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__app_services_orden__["a" /* Orden */]])
    ], ListadoOrdenes);
    return ListadoOrdenes;
}());

//# sourceMappingURL=listadoOrdenes.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Promociones; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_services_promociones__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Promociones = /** @class */ (function () {
    function Promociones(navCtrl, toastCtrl, navParams, storage, promocionesService) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.promocionesService = promocionesService;
        this.dataPromociones = [];
        this.obtenerPromociones();
        console.log(this.dataPromociones);
    }
    Promociones.prototype.obtenerPromociones = function () {
        var _this = this;
        this.promocionesService.promociones()
            .then(function (data) {
            console.log(data);
            _this.dataPromociones = data['promociones'];
        }).catch(function (error) {
            _this.toastCtrl.create({ message: 'Actualmente no hay promociones ', duration: 5000 }).present();
        });
    };
    Promociones = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-promociones',template:/*ion-inline-start:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\promociones\promociones.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title style="font-size: 2rem;">\n      Promociones\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <ion-slides pager>\n        <ion-slide *ngFor="let slide of dataPromociones">\n          <!--<ion-toolbar>\n            <ion-buttons end>\n              <button ion-button color="primary">Siguiente</button>\n            </ion-buttons>\n          </ion-toolbar>-->\n          <img src="http://servibucaros.com/cpanel/public{{slide.ruta }}" class="slide-image"/>\n          <!--<h2 class="slide-title" [innerHTML]="slide.title"></h2>\n          <p [innerHTML]="slide.description"></p>-->\n        </ion-slide>\n        <!--<ion-slide>\n          <ion-toolbar>\n          </ion-toolbar>\n          <img src="assets/img/ica-slidebox-img-4.png" class="slide-image"/>\n          <h2 class="slide-title">Ready to Play?</h2>\n          <button ion-button large clear icon-end color="primary">\n            Continue\n            <ion-icon name="arrow-forward"></ion-icon>\n          </button>\n        </ion-slide>-->\n      </ion-slides>\n</ion-content>\n'/*ion-inline-end:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\promociones\promociones.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__app_services_promociones__["a" /* PromocionesService */]])
    ], Promociones);
    return Promociones;
}());

//# sourceMappingURL=promociones.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PromocionesService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PromocionesService = /** @class */ (function () {
    function PromocionesService(http) {
        this.http = http;
        //private rutaApi ='http://192.168.1.27:8000/api/'; // ip de equipo local
        this.rutaApi = 'http://servibucaros.com/cpanel/api/'; // ip de produción
    }
    PromocionesService.prototype.promociones = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        return this.http
            .get(this.rutaApi + 'obtenerPromocionesActivas', { headers: headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    PromocionesService.prototype.handleError = function (error) {
        console.error('Error', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    PromocionesService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], PromocionesService);
    return PromocionesService;
}());

//# sourceMappingURL=promociones.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Galeria; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_services_galeria__ = __webpack_require__(202);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Galeria = /** @class */ (function () {
    function Galeria(navCtrl, toastCtrl, navParams, storage, galeriaService) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.galeriaService = galeriaService;
        this.dataGaleria = [];
        this.obtenerPromociones();
        console.log(this.dataGaleria);
    }
    Galeria.prototype.obtenerPromociones = function () {
        var _this = this;
        this.galeriaService.fotografias()
            .then(function (data) {
            console.log(data);
            _this.dataGaleria = data['fotografias'];
        }).catch(function (error) {
            _this.toastCtrl.create({ message: 'Actualmente no hay fotografias ', duration: 5000 }).present();
        });
    };
    Galeria = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-galeria',template:/*ion-inline-start:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\galeria\galeria.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title style="font-size: 2rem;">\n      Galeria\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>    \n    <ion-slides pager>\n        <ion-slide *ngFor="let slide of dataGaleria">\n          <!--<ion-toolbar>\n            <ion-buttons end>\n              <button ion-button color="primary">Siguiente</button>\n            </ion-buttons>\n          </ion-toolbar>-->\n          <img [src]="slide.ruta" class="slide-image"/>\n          <h2 class="slide-title" [innerHTML]="slide.title">Fotografia</h2>\n          <p [innerHTML]="slide.description">description de fotografia</p>\n        </ion-slide>        \n      </ion-slides> \n</ion-content>\n'/*ion-inline-end:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\galeria\galeria.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__app_services_galeria__["a" /* GaleriaService */]])
    ], Galeria);
    return Galeria;
}());

//# sourceMappingURL=galeria.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GaleriaService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GaleriaService = /** @class */ (function () {
    function GaleriaService(http) {
        this.http = http;
        //private rutaApi ='http://192.168.1.27:8000/api/'; // ip de equipo local
        this.rutaApi = 'http://servibucaros.com/cpanel/api/'; // ip de produción
    }
    GaleriaService.prototype.fotografias = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        return this.http
            .get(this.rutaApi + 'obtenerFotografias', { headers: headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    GaleriaService.prototype.handleError = function (error) {
        console.error('Error', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    GaleriaService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], GaleriaService);
    return GaleriaService;
}());

//# sourceMappingURL=galeria.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Quienes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { GaleriaService } from '../../app/services/galeria';
var Quienes = /** @class */ (function () {
    function Quienes(navCtrl, toastCtrl, navParams, storage
        //,public galeriaService:GaleriaService,   
    ) {
        //this.obtenerPromociones();
        //console.log(this.dataGaleria);
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.dataGaleria = [];
    }
    Quienes = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-quienes',template:/*ion-inline-start:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\quienes\quienes.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title style="font-size: 2rem;">\n      Quienes Somos\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <p style="font-size: 2.5rem;margin: 0;text-align: justify;font: oblique bold 120% cursive;">\n      Somos una empresa familiar que ofrece a la sociedad de Neiva un servicio de calidad basado en la excelencia, el cual consiste en un servicio de limpieza integral de automóviles y motos; y a través de una atención amable, rápida e impecable, obtener la satisfacción total de nuestros clientes.\n    </p>\n</ion-content>\n'/*ion-inline-end:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\quienes\quienes.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]
            //,public galeriaService:GaleriaService,   
        ])
    ], Quienes);
    return Quienes;
}());

//# sourceMappingURL=quienes.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Contactenos; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Contactenos = /** @class */ (function () {
    function Contactenos(navCtrl, platform) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.coords = {
            lat: 1,
            lng: 1
        };
    }
    Contactenos.prototype.ionViewWillEnter = function () {
        this.coords.lat = 2.9311795;
        this.coords.lng = -75.2979601;
        this.montarMapa();
        console.log('ionViewDidLoad MapaPage');
    };
    Contactenos.prototype.montarMapa = function () {
        var mapContainer = document.getElementById('map');
        this.map = new google.maps.Map(mapContainer, {
            center: this.coords,
            zoom: 50
        });
        var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
        var beachMarker = new google.maps.Marker({
            position: { lat: 2.9311795, lng: -75.2979601 },
            map: this.map,
            icon: image
        });
    };
    Contactenos = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contactenos',template:/*ion-inline-start:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\contactenos\contactenos.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title style="font-size: 2rem;">\n      Contactenos\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-card>\n        <ion-card-header style="background: #FF7000;color:white">\n          Contactos\n        </ion-card-header>\n        <ion-card-content padding>\n          Telefono : 8711319<br>\n          Avenida Circunvalar # 8 – 48 <br>\n          Barrio Los Martires <br>\n          Neiva - Huila\n        </ion-card-content>\n      </ion-card>\n     <a href="https://www.google.com/maps/place/Cra.+1f+%238-2,+Neiva,+Huila/@2.92514,-75.293411,16z/data=!4m5!3m4!1s0x8e3b747d9b969fa3:0xb1d58a1a0fd79ffe!8m2!3d2.9251401!4d-75.2934108?hl=es-ES">\n      <ion-img  height="300" src="http://servibucaros.com/cpanel/public/images/app/maps.png" cache="true" style="border-radius: 8px;width: 100%"></ion-img>\n      </a>\n</ion-content>\n'/*ion-inline-end:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\contactenos\contactenos.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */]])
    ], Contactenos);
    return Contactenos;
}());

//# sourceMappingURL=contactenos.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReservaService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReservaService = /** @class */ (function () {
    function ReservaService(http) {
        this.http = http;
        //private rutaApi ='http://192.168.1.27:8000/api/'; // ip de equipo local
        this.rutaApi = 'http://servibucaros.com/cpanel/api/'; // ip de produción
    }
    ReservaService.prototype.realizarReserva = function (data) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        return this.http
            .post(this.rutaApi + 'realizarReserva', JSON.stringify(data), { headers: headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ReservaService.prototype.handleError = function (error) {
        console.error('Error', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    ReservaService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], ReservaService);
    return ReservaService;
}());

//# sourceMappingURL=reserva.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(229);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_listadoOrdenes_listadoOrdenes__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_promociones_promociones__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_galeria_galeria__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_quienes_quienes__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_contactenos_contactenos__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_reservas_reservas__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__app_services_orden__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__app_services_promociones__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__app_services_galeria__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__app_services_reserva__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_status_bar__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_splash_screen__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
//ANGULAR




// IONIC










// Servicios






var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_listadoOrdenes_listadoOrdenes__["a" /* ListadoOrdenes */],
                __WEBPACK_IMPORTED_MODULE_9__pages_promociones_promociones__["a" /* Promociones */],
                __WEBPACK_IMPORTED_MODULE_10__pages_galeria_galeria__["a" /* Galeria */],
                __WEBPACK_IMPORTED_MODULE_11__pages_quienes_quienes__["a" /* Quienes */],
                __WEBPACK_IMPORTED_MODULE_12__pages_contactenos_contactenos__["a" /* Contactenos */],
                __WEBPACK_IMPORTED_MODULE_13__pages_reservas_reservas__["a" /* Reservas */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_listadoOrdenes_listadoOrdenes__["a" /* ListadoOrdenes */],
                __WEBPACK_IMPORTED_MODULE_9__pages_promociones_promociones__["a" /* Promociones */],
                __WEBPACK_IMPORTED_MODULE_10__pages_galeria_galeria__["a" /* Galeria */],
                __WEBPACK_IMPORTED_MODULE_11__pages_quienes_quienes__["a" /* Quienes */],
                __WEBPACK_IMPORTED_MODULE_12__pages_contactenos_contactenos__["a" /* Contactenos */],
                __WEBPACK_IMPORTED_MODULE_13__pages_reservas_reservas__["a" /* Reservas */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_14__app_services_orden__["a" /* Orden */],
                __WEBPACK_IMPORTED_MODULE_15__app_services_promociones__["a" /* PromocionesService */],
                __WEBPACK_IMPORTED_MODULE_16__app_services_galeria__["a" /* GaleriaService */],
                __WEBPACK_IMPORTED_MODULE_17__app_services_reserva__["a" /* ReservaService */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["a" /* IonicStorageModule */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, storage) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.storage = storage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */];
        this.validarAutenticado();
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */] }
        ];
    }
    MyApp.prototype.validarAutenticado = function () {
        var _this = this;
        this.storage.get('placa').then(function (val) {
            console.log(val);
            if (val != null && val != '' && val != 'null') {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */];
            }
        });
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\app\app.html"*/'<!--<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>    \n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" ></ion-nav>'/*ion-inline-end:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Reservas; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_services_reserva__ = __webpack_require__(205);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Reservas = /** @class */ (function () {
    function Reservas(navCtrl, toastCtrl, navParams, storage, reserva) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.reserva = reserva;
        this.descripcion = '';
        this.storage.get('placa').then(function (val) {
            _this.placa = val;
        });
    }
    Reservas.prototype.realizarReserva = function () {
        var _this = this;
        if (this.validarCamposVacios() == false) {
            return false;
        }
        var data = {
            placa: this.placa,
            fecha: this.fecha,
            nombre: this.nombre,
            telefono: this.telefono,
            descripcion: this.descripcion
        };
        this.reserva.realizarReserva(data)
            .then(function (data) {
            console.log(data);
            _this.limpiarCampos();
            _this.toastCtrl.create({ message: data['mensaje'], duration: 5000 }).present();
        }).catch(function (error) {
            _this.toastCtrl.create({ message: data['mensaje'], duration: 5000 }).present();
        });
    };
    Reservas.prototype.validarCamposVacios = function () {
        console.log(this.fecha);
        console.log(this.nombre);
        console.log(this.telefono);
        if (this.fecha == undefined || this.fecha == '') {
            this.toastCtrl.create({ message: 'Ingrese la fecha de la reserva', duration: 5000 }).present();
            return false;
        }
        if (this.nombre == undefined || this.nombre == '') {
            this.toastCtrl.create({ message: 'Ingrese el nombre de quien reserva', duration: 5000 }).present();
            return false;
        }
        if (this.telefono == undefined || this.telefono == '') {
            this.toastCtrl.create({ message: 'Ingrese el telefono de quien reserva', duration: 5000 }).present();
            return false;
        }
    };
    Reservas.prototype.limpiarCampos = function () {
        this.fecha = '';
        this.nombre = '';
        this.telefono = '';
        this.descripcion = '';
    };
    Reservas = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reservas',template:/*ion-inline-start:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\reservas\reservas.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title style="font-size: 2rem;">\n      Reservas\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <ion-item>\n        <ion-label floating>Fecha y hora </ion-label>\n        <ion-input type="datetime-local" [(ngModel)]="fecha"></ion-input>\n      </ion-item>\n      <ion-item>\n          <ion-label floating>Nombre</ion-label>\n          <ion-input type="text" [(ngModel)]="nombre"></ion-input>\n      </ion-item>\n      <ion-item>\n          <ion-label floating>Telefono</ion-label>\n          <ion-input type="number" [(ngModel)]="telefono"></ion-input>\n      </ion-item>\n      <ion-item>\n          <textarea rows="4" cols="50"  [(ngModel)]="descripcion" ></textarea>\n\n      </ion-item>\n      <ion-row>\n          <ion-col>\n            <button ion-button round class="submit-btn btn-general" full (click)="realizarReserva()" style="background:#FF7000">Reservar <!-- [disabled]="!loginForm.form.valid" -->\n            </button>\n          </ion-col>\n        </ion-row>\n</ion-content>\n'/*ion-inline-end:"C:\Users\edwinsamir\Documents\Apps\servibucarosapp\src\pages\reservas\reservas.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__app_services_reserva__["a" /* ReservaService */]])
    ], Reservas);
    return Reservas;
}());

//# sourceMappingURL=reservas.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Orden; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Orden = /** @class */ (function () {
    function Orden(http) {
        this.http = http;
        //private rutaApi ='http://192.168.1.27:8000/api/'; // ip de equipo local
        this.rutaApi = 'http://servibucaros.com/cpanel/api/'; // ip de produción
    }
    Orden.prototype.verificarPlaca = function (data) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        return this.http
            .post(this.rutaApi + 'verificarPlaca', JSON.stringify(data), { headers: headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    Orden.prototype.obtenerOrdenes = function (data) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        return this.http
            .post(this.rutaApi + 'ordenesSinReclamar', JSON.stringify(data), { headers: headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    Orden.prototype.handleError = function (error) {
        console.error('Error', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    Orden = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], Orden);
    return Orden;
}());

//# sourceMappingURL=orden.js.map

/***/ })

},[206]);
//# sourceMappingURL=main.js.map