import { Component, ViewChild } from '@angular/core';
import { LoadingController, NavController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Orden } from '../../app/services/orden';
import { HomePage } from '../../pages/home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  //@ViewChild('placa') placa: any;
  private placa: string;
  private error: string;

  constructor(
    private navCtrl: NavController,
    private toastCtrl:ToastController,
    private storage: Storage,
    public orden:Orden
  ) {

  }

  ionViewDidLoad(): void {
    /*setTimeout(() => {

    }, 500);*/
  }

  verificarPlaca() {
    let data = {'placa':this.placa};
    this.orden.verificarPlaca(data)
    .then(data => {
      console.log(data);
      this.storage.set('placa', this.placa);
      console.log(this.storage);
      console.log(this.storage.get('placa'));
      this.navCtrl.setRoot(HomePage);
    }).catch(error =>{
      this.toastCtrl.create({message: 'No hay un vehiculo con esa placa', duration: 5000}).present();
    });
  }
}
