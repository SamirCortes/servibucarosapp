import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ReservaService } from '../../app/services/reserva';
import { HomePage } from '../../pages/home/home';

@Component({
  selector: 'page-reservas',
  templateUrl: 'reservas.html'
})
export class Reservas {

  private fecha;
  private nombre;
  private telefono;
  private descripcion = '';
  private placa;

  constructor(
    public navCtrl: NavController,
    private toastCtrl:ToastController,
    private navParams:NavParams,
    private storage: Storage,
    private reserva: ReservaService

  ) {
    this.storage.get('placa').then((val) => {
      this.placa = val;
    });

  }

  realizarReserva(){
      if(this.validarCamposVacios() == false){
        return false;
      }
      let data = {
          placa:this.placa,
          fecha:this.fecha,
          nombre:this.nombre,
          telefono:this.telefono,
          descripcion:this.descripcion
      }
      this.reserva.realizarReserva(data)
      .then(data => {
        console.log(data);
        this.limpiarCampos();
        this.toastCtrl.create({message: data['mensaje'], duration: 5000}).present();
      }).catch(error =>{
        this.toastCtrl.create({message: data['mensaje'], duration: 5000}).present();
      });

  }

  validarCamposVacios(){
    console.log(this.fecha);
    console.log(this.nombre);
    console.log(this.telefono);
    if(this.fecha == undefined || this.fecha == ''){
      this.toastCtrl.create({message: 'Ingrese la fecha de la reserva', duration: 5000}).present();
      return false;
    }
    if(this.nombre == undefined || this.nombre == ''){
      this.toastCtrl.create({message: 'Ingrese el nombre de quien reserva', duration: 5000}).present();
      return false;
    }
    if(this.telefono == undefined || this.telefono == ''){
      this.toastCtrl.create({message: 'Ingrese el telefono de quien reserva', duration: 5000}).present();
      return false;
    }
  }
  limpiarCampos(){
    this.fecha = '';
    this.nombre = '';
    this.telefono = '';
    this.descripcion = '';
  }

}
