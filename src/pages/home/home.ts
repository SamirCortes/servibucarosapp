import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Orden } from '../../app/services/orden';
import { ListadoOrdenes } from '../listadoOrdenes/listadoOrdenes';
import { LoginPage } from '../login/login';
import { Promociones } from '../promociones/promociones';
import { Galeria } from '../galeria/galeria';
import { Quienes } from '../quienes/quienes';
import { Contactenos } from '../contactenos/contactenos';
import { Reservas } from '../reservas/reservas';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private placa;
  private cantidad = 0;

  constructor(
    public navCtrl: NavController,
    private toastCtrl:ToastController,
    private storage: Storage,
    public orden:Orden
  ) {
    console.log(this.storage);
    this.storage.get('placa').then((val) => {
      this.placa = val;
      this.cargarOrdenes();
    });

  }

  listadoOrdenes(){
    this.navCtrl.push(ListadoOrdenes);
  }

  promociones(){
    this.navCtrl.push(Promociones);
  }

  galeria(){
    this.navCtrl.push(Galeria);
  }

  quienes(){
    this.navCtrl.push(Quienes);
  }

  contactenos(){
    this.navCtrl.push(Contactenos);
  }

  reservas(){
    this.toastCtrl.create({message: 'Las reservas estan inactivas', duration: 5000}).present();
    //this.navCtrl.push(Reservas);
  }

  salir(){
    this.storage.remove('placa');
    this.navCtrl.setRoot(LoginPage);
  }

  cargarOrdenes() {
    let data = {'placa': this.placa};
    this.orden.obtenerOrdenes(data)
    .then(data => {
      console.log(data);
      this.cantidad = data['ordenes'].length;
      console.log(this.cantidad);
    }).catch(error =>{
      this.toastCtrl.create({message: 'El vehiculo no tiene servicios por reclamar', duration: 7000}).present();
    });
  }
}
