import { Component } from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
//import { GaleriaService } from '../../app/services/galeria';

declare var google: any;

@Component({
  selector: 'page-contactenos',
  templateUrl: 'contactenos.html'
})
export class Contactenos {
  private map: any; // Manejador del mapa
  private coords = {
      lat : 1,
      lng:1
  }

  constructor(public navCtrl: NavController, public platform: Platform) {

  }

  ionViewWillEnter() {
      this.coords.lat= 2.9311795;
      this.coords.lng = -75.2979601;
      this.montarMapa();
      console.log('ionViewDidLoad MapaPage');
  }

  public montarMapa() {
      let mapContainer = document.getElementById('map');
      this.map = new google.maps.Map(mapContainer,{
          center: this.coords,
          zoom: 50
      });

      let image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
        let beachMarker = new google.maps.Marker({
            position: {lat: 2.9311795, lng: -75.2979601},
            map: this.map,
            icon: image
        });
  }

}
