import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { PromocionesService } from '../../app/services/promociones';

@Component({
  selector: 'page-promociones',
  templateUrl: 'promociones.html'
})
export class Promociones {

  private dataPromociones:any = [];

  constructor(
    public navCtrl: NavController,
    private toastCtrl:ToastController,
    private navParams:NavParams,
    private storage: Storage,
    public promocionesService:PromocionesService,   

  ) {        
    this.obtenerPromociones();
    console.log(this.dataPromociones);

  }  

  obtenerPromociones() {
    this.promocionesService.promociones()
    .then(data => {    
      console.log(data);
      this.dataPromociones = data['promociones'];      
    }).catch(error =>{
      this.toastCtrl.create({message: 'Actualmente no hay promociones ', duration: 5000}).present();      
    });
  }

}
