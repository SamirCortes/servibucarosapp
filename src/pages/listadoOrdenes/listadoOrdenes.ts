import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Orden } from '../../app/services/orden';

@Component({
  selector: 'page-listadoOrdenes',
  templateUrl: 'listadoOrdenes.html'
})
export class ListadoOrdenes {

  private cantidad = 0;
  private ordenes = [];
  private placa;

  constructor(
    public navCtrl: NavController,
    private toastCtrl:ToastController,
    private navParams:NavParams,
    private storage: Storage,
    public orden:Orden
  ) {
    this.storage.get('placa').then((val) => {
      this.placa = val;
      this.cargarOrdenes();
    });


  }

  cargarOrdenes() {
    let data = {'placa': this.placa};
    this.orden.obtenerOrdenes(data)
    .then(data => {
      console.log(data);
      this.ordenes = data['ordenes'];
      this.cantidad = data['ordenes'].length;
    }).catch(error =>{
      this.toastCtrl.create({message: 'El vehiculo no tiene servicios por reclamar', duration: 5000}).present();
    });
  }
}
