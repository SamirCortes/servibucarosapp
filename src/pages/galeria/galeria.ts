import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GaleriaService } from '../../app/services/galeria';

@Component({
  selector: 'page-galeria',
  templateUrl: 'galeria.html'
})
export class Galeria {

  private dataGaleria:any = [];

  constructor(
    public navCtrl: NavController,
    private toastCtrl:ToastController,
    private navParams:NavParams,
    private storage: Storage,
    public galeriaService:GaleriaService,   

  ) {        
    this.obtenerPromociones();
    console.log(this.dataGaleria);

  }  

  obtenerPromociones() {
    this.galeriaService.fotografias()
    .then(data => {    
      console.log(data);
      this.dataGaleria = data['fotografias'];      
    }).catch(error =>{
      this.toastCtrl.create({message: 'Actualmente no hay fotografias ', duration: 5000}).present();      
    });
  }

}
