//ANGULAR
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

// IONIC
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { ListadoOrdenes } from '../pages/listadoOrdenes/listadoOrdenes';
import { Promociones } from '../pages/promociones/promociones';
import { Galeria } from '../pages/galeria/galeria';
import { Quienes } from '../pages/quienes/quienes';
import { Contactenos } from '../pages/contactenos/contactenos';
import { Reservas } from '../pages/reservas/reservas';

// Servicios
import { Orden } from '../app/services/orden';
import { PromocionesService } from '../app/services/promociones';
import { GaleriaService } from '../app/services/galeria';
import { ReservaService } from '../app/services/reserva';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    ListadoOrdenes,
    Promociones,
    Galeria,
    Quienes,
    Contactenos,
    Reservas
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    ListadoOrdenes,
    Promociones,
    Galeria,
    Quienes,
    Contactenos,
    Reservas
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Orden,
    PromocionesService,
    GaleriaService,
    ReservaService,
    IonicStorageModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
