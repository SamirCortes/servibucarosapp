import {Injectable} from '@angular/core';
import {Http,Headers, RequestOptions} from '@angular/http';

@Injectable()
export class Orden{

  //private rutaApi ='http://192.168.1.27:8000/api/'; // ip de equipo local

  private rutaApi ='http://servibucaros.com/cpanel/api/'; // ip de produción

  constructor(private http:Http){
  }

  verificarPlaca(data): Promise<Orden> {
    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http
      .post(this.rutaApi+'verificarPlaca', JSON.stringify(data), {headers: headers})
      .toPromise()
      .then(res => res.json() as Orden)
      .catch(this.handleError);
  }

  obtenerOrdenes(data): Promise<Orden> {
    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http
      .post(this.rutaApi+'ordenesSinReclamar', JSON.stringify(data), {headers: headers})
      .toPromise()
      .then(res => res.json() as Orden)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
