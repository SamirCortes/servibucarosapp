import {Injectable} from '@angular/core';
import {Http,Headers, RequestOptions} from '@angular/http';

@Injectable()
export class PromocionesService{

  //private rutaApi ='http://192.168.1.27:8000/api/'; // ip de equipo local

  private rutaApi ='http://servibucaros.com/cpanel/api/'; // ip de produción

  constructor(private http:Http){
  }

  promociones(): Promise<PromocionesService> {
    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http
      .get(this.rutaApi+'obtenerPromocionesActivas', {headers: headers})
      .toPromise()
      .then(res => res.json() as PromocionesService)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
