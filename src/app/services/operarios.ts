import {Injectable} from '@angular/core';
import {Http,Headers, RequestOptions} from '@angular/http';

@Injectable()
export class Operarios{

  //private rutaApi ='http://192.168.1.13:8000/api/'; // ip de equipo local

  private rutaApi ='http://servibucaros.com/cpanel/api/'; // ip de produción

  constructor(private http:Http){
  }

  loginService(data): Promise<Operarios> {
    return this.http
      .get(this.rutaApi+'obtenerOperarios')
      .toPromise()
      .then(res => res.json() as Operarios)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
