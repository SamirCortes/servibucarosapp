import {Injectable} from '@angular/core';
import {Http,Headers, RequestOptions} from '@angular/http';

@Injectable()
export class AuthService{

  //private rutaApi ='http://192.168.43.25:8000/api/'; // ip de equipo local

  private rutaApi ='http://servibucaros.com/cpanel/api/'; // ip de produción

  constructor(private http:Http){
  }

  loginService(data): Promise<AuthService> {
    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http
      .post(this.rutaApi+'login', JSON.stringify(data), {headers: headers})
      .toPromise()
      .then(res => res.json() as AuthService)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
